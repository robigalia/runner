#!/usr/bin/env bash
. ~/.profile
git clone https://gitlab.com/robigalia/sel4-sys --recursive
git clone https://gitlab.com/robigalia/sel4-config
git clone https://gitlab.com/robigalia/sel4 --recursive
pushd ../sel4
make x64_qemu_defconfig
make
popd
cd sel4-sys
env RUST_BACKTRACE=1 xargo build --verbose --target x86_64-sel4-robigalia
