FROM debian:stretch
MAINTAINER Corey Richardson "corey@octayn.net"
ENV DEBIAN_FRONTEND=noninteractive PATH=/root/.cargo/bin:$PATH RUST_TARGET_PATH=/sel4-targets
RUN dpkg --add-architecture i386 && \
dpkg --add-architecture armhf && \
dpkg --add-architecture armel && \
apt-get update -y && \
apt-get upgrade -y && \
apt-get install -y curl python python-pip python-setuptools python-wheel libxml2 \
        build-essential git clang crossbuild-essential-armhf \
        crossbuild-essential-armel llvm libc6-dev:i386 \
        git cmake cmake-data pkg-config libelf-dev libdw-dev elfutils \
        libcurl4-openssl-dev binutils-dev libz-dev libiberty-dev jq \
        libxml2-utils libxml2-dev libxslt-dev python-dev --no-install-recommends && \
pip install sel4-deps && \
rm -rf /usr/share/locale/* && \
rm -rf /var/cache/debconf/*-old && \
rm -rf /var/lib/apt/lists/* && \
rm -rf /usr/share/doc/* && \
git clone https://gitlab.com/robigalia/sel4-targets /sel4-targets && \
git clone https://github.com/SimonKagstrom/kcov.git && \
cd kcov && \
mkdir build && \
cd build && \
cmake .. && \
make && \
make install && \
cd ../../ && \
rm -rf kcov && \
curl https://sh.rustup.rs -sSf | sh -s -- -y --default-toolchain nightly-2018-05-31 && \
rustup component add rust-src && \
cargo install xargo
COPY smoke.sh /
