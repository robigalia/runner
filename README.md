# runner

This repository contains the Dockerfile for our [GitLab CI
runner](https://gitlab.com/gitlab-org/gitlab-ci-multi-runner).
It also keeps a copy of the target files from sel4-targets.

If sel4-targets gets changed, this container needs to be rebuilt.
